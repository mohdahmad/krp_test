/*
   sample schedule() hook to create my process_list for ROP protection
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/kprobes.h>
#include <linux/string.h>
#include <linux/slab.h>
#include <linux/kallsyms.h>
#include <linux/sched.h>
#include <linux/cgroup.h>
#include <linux/time.h>

/*
   ints in 1 MB = 262144 
   setting to 2MB
 */
#define L3_SIZE       524288 

typedef struct {
    struct task_struct *task_struct; /* pointer to the process task struct */
    unsigned long identifier; /* we use this to match the task struct to our process */
    struct process_list *next;
} process_list;

process_list *list_head, *list_tail, *list_current, *list_next;

int* intArray;
static struct kretprobe kp_ktest_return;

struct cgroup_subsys_state;

struct cfs_bandwidth {
#ifdef CONFIG_CFS_BANDWIDTH
    raw_spinlock_t lock;
    ktime_t period;
    u64 quota, runtime;
    s64 hierarchal_quota;
    u64 runtime_expires;

    int idle, timer_active;
    struct hrtimer period_timer, slack_timer;
    struct list_head throttled_cfs_rq;

    /* statistics */
    int nr_periods, nr_throttled;
    u64 throttled_time;
#endif
};

struct task_group {
    struct cgroup_subsys_state css;

#ifdef CONFIG_FAIR_GROUP_SCHED
    /* schedulable entities of this group on each cpu */
    struct sched_entity **se;
    /* runqueue "owned" by this group on each cpu */
    struct cfs_rq **cfs_rq;
    unsigned long shares;

#ifdef  CONFIG_SMP
    atomic_long_t load_avg;
    atomic_t runnable_avg;
#endif
#endif

#ifdef CONFIG_RT_GROUP_SCHED
    struct sched_rt_entity **rt_se;
    struct rt_rq **rt_rq;

    struct rt_bandwidth rt_bandwidth;
#endif

    struct rcu_head rcu;
    struct list_head list;

    struct task_group *parent;
    struct list_head siblings;
    struct list_head children;

#ifdef CONFIG_SCHED_AUTOGROUP
    struct autogroup *autogroup;
#endif

    struct cfs_bandwidth cfs_bandwidth;
};

static int handler_ktest_return(struct kretprobe_instance *i, struct pt_regs *regs)
{

    int cpu = current->wake_cpu;
    struct task_group *tgroup = current->sched_task_group;
    struct cgroup_subsys_state css = tgroup->css;

    if (css.cgroup != NULL){ 
        printk(KERN_INFO "CPU: %d PID: %d CGROUP_ID: %d ", cpu, current->pid, css.cgroup->id);
    } else {
        printk(KERN_INFO "PID:%d CPU: %d CGROUP_ID: NULL", current->pid, cpu);
    }

    return 0;
}

static int handler_fault(struct kprobe *p, struct pt_regs *regs, int trapnr)
{
    printk(KERN_INFO "fault_handler: p->addr = 0x%p, trap #%dn",
            p->addr, trapnr);

    /* we don't handle the fault. */
    return 0;
}

static int __init kprobe_init(void)
{
    int ret;

    kp_ktest_return.maxactive=NR_CPUS;
    kp_ktest_return.handler=handler_ktest_return;
    kp_ktest_return.kp.symbol_name="schedule";
    kp_ktest_return.kp.fault_handler=handler_fault;

    ret = register_kretprobe(&kp_ktest_return);

    if (ret < 0) {
        printk(KERN_INFO "register_kprobe failed, returned %d\n", ret);
        return ret;
    }
    printk(KERN_INFO "Planted kretprobe at %p\n", kp_ktest_return.kp.addr);

    return 0;
}

static void __exit kprobe_exit(void)
{
    unregister_kretprobe(&kp_ktest_return);
    printk(KERN_INFO "kprobe at %p unregistered\n", kp_ktest_return.kp.addr);
}

/*
   static int handler_sched_return(struct kretprobe_instance *i, struct pt_regs *regs)
   {
   int volatile v;
   unsigned int j;
   int preCgroup = -1;
   int c = current->wake_cpu;
   struct task_group *tgroup = current->sched_task_group;
   struct cgroup_subsys_state css = tgroup->css;

   if (c==8){  
   if ((css.cgroup != NULL) && (css.cgroup->id != 1)){
   printk(KERN_INFO "PID:%d CPU: %d CGROUP_ID: %d ", current->pid, c, css.cgroup->id);
   if (css.cgroup->id != preCgroup){
   for (j = 0; j < L3_SIZE; j += 16 ){
   v = intArray[j];
   }
   preCgroup = css.cgroup->id;
   }
   } 
   }
   return 0;
   }
 */

/*
 * called if an exception is generated for any {pre|post} handler
 * or single-step
 */


    module_init(kprobe_init)
module_exit(kprobe_exit)
    MODULE_LICENSE("GPL");
